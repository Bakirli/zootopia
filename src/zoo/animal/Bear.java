package zoo.animal;

import java.util.Arrays;
import java.util.StringJoiner;

public class Bear extends Animal {
    public Bear() {
        this.eats = new String[]{"fish", "steak"};
        this.lifeExpectancy = 18;
    }

    @Override
    public void treat() {
        hug();
    }

    private void hug() {
        System.out.println("bear wants a hug ");
        setHealth(getHealth() + 3);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Bear.class.getSimpleName() + "[", "]")
                .add("age=" + age)
                .add("gender='" + gender + "'")
                .add("eats=" + Arrays.toString(eats))
                .add("health=" + health)
                .add("lifeExpectancy=" + lifeExpectancy)
                .toString();
    }
}
