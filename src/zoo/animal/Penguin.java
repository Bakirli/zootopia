package zoo.animal;

import java.util.Arrays;
import java.util.StringJoiner;

public class Penguin extends Animal {
    public Penguin() {
        this.lifeExpectancy = 15;
        this.eats = new String[]{"fish", "icecream"};
    }

    @Override
    public void treat() {
        watchAFilm();
    }

    private void watchAFilm() {
        System.out.println("penguin is watching masha and medved");
        setHealth(getHealth() + 2);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Penguin.class.getSimpleName() + "[", "]")
                .add("age=" + age)
                .add("gender='" + gender + "'")
                .add("eats=" + Arrays.toString(eats))
                .add("health=" + health)
                .add("lifeExpectancy=" + lifeExpectancy)
                .toString();
    }
}
