package zoo.animal;

import zoo.enclosure.Enclosure;
import zoo.food.Food;
import zoo.food.FoodMenu;

import java.util.Arrays;
import java.util.StringJoiner;

public abstract class Animal {
    protected int age;
    protected String gender;
    protected String[] eats;
    protected int health;
    protected int lifeExpectancy;
    protected Enclosure enclosure;

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public void setEnclosure(Enclosure enclosure) {
        this.enclosure = enclosure;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String[] getEats() {
        return eats;
    }

    public void setEats(String[] eats) {
        this.eats = eats;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        if(health >= 0 && health <= 10) {
            this.health = health;
        }
//        else {
//            System.out.println("Invalid health " + health);
//        }
    }

    public int getLifeExpectancy() {
        return lifeExpectancy;
    }

    public void setLifeExpectancy(int lifeExpectancy) {
        this.lifeExpectancy = lifeExpectancy;
    }

    public boolean canEat(String food) {
        boolean valid = false;

        for (String f : eats) {
            if(f.equals(food)) {
                valid = true;
                break;
            }
        }
        return valid;
    }

    public void eat() {
        System.out.println("animal is eating");
    }

    public void decreaseHealth() {
        setHealth(health - 1);
    }

    public abstract void treat();

    public boolean aMonthPasses() {
        System.out.println("a month passes for animal " + this);

        if(age < lifeExpectancy) {
            this.age++;
            for (String food: eats) {
                if(this.enclosure.getFoodstore().takeFood(food)) {
                    System.out.println("animal eats " + food);

                    // increase health
                    Food foodObject = FoodMenu.getFood(food);
                    setHealth(this.getHealth() + foodObject.getHealth());
                    System.out.println("animal health increased by " + foodObject.getHealth());

                    // increase waste in enclosure
                    this.enclosure.addWaste(foodObject.getWaste());
                    System.out.println("enclosure waste increased by " + foodObject.getWaste());
                    break;
                }
            }

            decreaseHealth();
            decreaseHealth();

            return true;
        } else {
            System.out.println("Ezrayil is coming for " + this);
            setHealth(0);
            return false;
        }

    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Animal.class.getSimpleName() + "[", "]")
                .add("age=" + age)
                .add("health=" + health)
                .add("gender=" + gender)
                .add("eats=" + Arrays.toString(eats))
                .add("lifeExpectancy=" + lifeExpectancy)
                .toString();
    }
}
