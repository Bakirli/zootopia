package zoo;

import zoo.animal.Lion;
import zoo.animal.Tiger;
import zoo.enclosure.Enclosure;

public class ZooMain {

    public static void main(String[] args) {
        Zoo zoo = new Zoo();

        Enclosure enclosure = new Enclosure();
        Lion lion = new Lion();
        lion.setAge(5);
        lion.setHealth(10);
        lion.setGender("f");

        Tiger tiger = new Tiger();
        tiger.setAge(10);
        tiger.setHealth(10);
        tiger.setGender("m");
        enclosure.addAnimal(lion);
        enclosure.addAnimal(tiger);

        Zookeeper zookeeper = new Zookeeper();
        zookeeper.setEnclosure(enclosure);
        zookeeper.setZoo(zoo);

        zoo.getEnclosures().add(enclosure);
        zoo.getZookeepers().add(zookeeper);
        zoo.go();
    }
}
