package zoo;

import zoo.animal.Animal;
import zoo.enclosure.Enclosure;
import zoo.food.FoodMenu;
import zoo.food.Foodstore;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Zoo {
    private List<Enclosure> enclosures;
    private Foodstore foodstore;
    private List<Zookeeper> zookeepers;

    public Zoo() {
        this.enclosures = new ArrayList<>();
        this.foodstore = new Foodstore();
        this.zookeepers = new ArrayList<>();
    }

    public List<Enclosure> getEnclosures() {
        return enclosures;
    }

    public void setEnclosures(List<Enclosure> enclosures) {
        this.enclosures = enclosures;
    }

    public Foodstore getFoodstore() {
        return foodstore;
    }

    public void setFoodstore(Foodstore foodstore) {
        this.foodstore = foodstore;
    }



    public boolean aMonthPasses() {
        System.out.println("a month passes in zoo");

        // The zoo Foodstore can be restocked.
        restock();

        //Each Zookeeper (once these have been implemented) will have aMonthPasses() called on it. This
        //will allow them to perform their duties.

        for(Zookeeper zookeeper : zookeepers) {
            zookeeper.aMonthPasses();
        }

        // Each Enclosure will have aMonthPasses() called on it. This will in turn then call the method on all
        //the Animals in the Enclosure.
        for (Enclosure enclosure : enclosures) {
            enclosure.aMonthPasses();
        }

        removeDeadAnimals();

        return true;
    }

    private void removeDeadAnimals() {
        // Each Animal in the zoo will be checked to see if its health is 0. If so, it will be removed from the zoo.
        List<Animal> deadAnimals = new ArrayList<>();

        for (Enclosure enclosure : enclosures) {
            for(Animal animal : enclosure.getAnimals()) {
                if(animal.getHealth() == 0) {
                    // remove animal
//                    System.out.println("removing dead " + animal + " from enclosure");
//                    enclosure.removeAnimal(animal);
                    deadAnimals.add(animal);
                }
            }
        }

        for (Animal animal : deadAnimals) {
            for (Enclosure enclosure : enclosures) {
                if(enclosure.removeAnimal(animal)) {
                    // remove animal
                    System.out.println("removed dead " + animal + " from enclosure");
                    break;
                }
            }
        }
    }

    private void restock() {
        System.out.println("\nrestock zoo foodstore");
        Set<String> foods = FoodMenu.getFoodNames();
        Random random = new Random();
        for (String food : foods)  {
            int count = random.nextInt(50);
            if(count > 0) {
                System.out.println("adding " + count + " " + food + " into zoo foodstore");
                this.foodstore.addFood(food, count);
            } else {
                System.out.println("no chance for " + food);
            }
        }

        System.out.println("zoo foodstore = " + foodstore);
    }

    public void go() {
        System.out.println("Starting Zoo Simulation");
        boolean runSimulation = true;
        int totalAnimalCount = 0;
        int month = 1;
        while (runSimulation) {
            totalAnimalCount = 0;
            System.out.println("\nrunning month " + month + "\n");

            for (Enclosure enclosure : enclosures) {
                totalAnimalCount += enclosure.size();
            }
            System.out.println("total animal count = " + totalAnimalCount);

            runSimulation = totalAnimalCount > 0;

            aMonthPasses();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            month++;
            System.out.println();
        }
    }

    public List<Zookeeper> getZookeepers() {
        return zookeepers;
    }

    public void setZookeepers(List<Zookeeper> zookeepers) {
        this.zookeepers = zookeepers;
    }
}
