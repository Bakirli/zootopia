package zoo;

import zoo.animal.Animal;
import zoo.enclosure.Enclosure;

import java.util.*;

public class Zookeeper {
    private Enclosure enclosure;
    private Zoo zoo;

    public Zookeeper() {
        System.out.println("creating general zookeeper");
    }

    private void restockEnclosureFoodStore() {
        Set<String> wishList = new HashSet<>();

        // prepare wish list, list of foods that animals can eat
        for (Animal animal : enclosure.getAnimals()) {
            wishList.addAll(Arrays.asList(animal.getEats()));
        }

        int foodLimit = 20;

        //Moving up to 20 items of food from the Zoo Foodstore to the Enclosure Foodstore.
        Random random = new Random();

        for(String food : wishList) {
            // equals random chance for all foods
            int foodCount = random.nextInt(foodLimit/wishList.size());
            System.out.println("zookeeper must move " + foodCount + " " + food + " from zoo foodstore into enclosure foodstore");

            // move food from zoo foodstore to enclosure food store
            int i = 0;
            for (i = 0; i < foodCount && zoo.getFoodstore().takeFood(food); i++) {
                enclosure.getFoodstore().addFood(food, 1);
//                System.out.println("zookeeper is moving " + food + " into enclosure foodstore");
            }

            if(i > 0) {
                System.out.println("zookeeper moved " + i + " " + food + " from zoo foodstore into enclosure foodstore");
            } else {
                System.out.println("zoo foodstore does not contain " + food);
            }
        }

        System.out.println("enclosure foodstore = " + enclosure.getFoodstore());
    }

    public boolean aMonthPasses() {
        restockEnclosureFoodStore();
        cleanupEnclosure();
        treatAnimals();
        return true;
    }

    private void treatAnimals() {

        if(enclosure.size() > 0) {
            //Giving a treat to up to two animals.
            Random random = new Random();
            int treatCount = random.nextInt(2);
            if(treatCount > 0) {
                while (treatCount > 0) {
                    int animalIndex = random.nextInt(enclosure.size());
                    enclosure.getAnimals().get(animalIndex).treat();
                    System.out.println("zookeeper is treating " + enclosure.getAnimals().get(animalIndex));
                    treatCount--;
                }
                System.out.println("zookeeper has treated " + treatCount + " animal");
            } else {
                System.out.println("no treatment in this month");
            }
        } else {
            System.out.println("all animals dead");
        }

    }

    private void cleanupEnclosure() {
        // Removing up to 20 units of waste from the Enclosure
        Random random = new Random();
        int wasteCount = random.nextInt(20);
        enclosure.removeWaste(wasteCount);
    }

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public void setEnclosure(Enclosure enclosure) {
        this.enclosure = enclosure;
    }

    public Zoo getZoo() {
        return zoo;
    }

    public void setZoo(Zoo zoo) {
        this.zoo = zoo;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Zookeeper.class.getSimpleName() + "[", "]")
                .add("enclosure=" + enclosure)
                .toString();
    }
}
