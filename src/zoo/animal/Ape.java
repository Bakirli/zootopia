package zoo.animal;

public abstract class Ape extends Animal {

    public Ape() {
        this.eats = new String[] {"fruit", "ice cream"};
    }
}
