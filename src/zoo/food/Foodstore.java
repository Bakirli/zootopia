package zoo.food;

import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

public class Foodstore {

    private Map<String, Integer> foods;

    public Foodstore() {
        this.foods = new HashMap<>();
    }

    public void addFood(String food, int quantity) {
        if (quantity > 0) {
            if (foods.containsKey(food)) {
                foods.put(food, quantity + foods.get(food));
            } else {
                foods.put(food, quantity);
            }
        } else {
            throw new IllegalArgumentException("Invalid quantity " + quantity);
        }
    }

    public boolean takeFood(String food) {
        if(foods.containsKey(food)) {
            int count = foods.get(food);
            System.out.println("foodstore contains " + count + " " + food);

            if(count > 1) {
                foods.put(food, count - 1);
                System.out.println("take 1 " + food);
            } else {
                foods.remove(food);
                System.out.println("remove last " + food + " from food store");
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Foodstore.class.getSimpleName() + "[", "]")
                .add("foods=" + foods)
                .toString();
    }
}
