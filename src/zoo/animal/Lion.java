package zoo.animal;

import java.util.Arrays;
import java.util.StringJoiner;

public class Lion extends BigCat {

    @Override
    protected void stroked() {
        this.setHealth(this.health + 2);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Lion.class.getSimpleName() + "[", "]")
                .add("age=" + age)
                .add("gender='" + gender + "'")
                .add("eats=" + Arrays.toString(eats))
                .add("health=" + health)
                .add("lifeExpectancy=" + lifeExpectancy)
                .toString();
    }
}
