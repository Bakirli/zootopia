package zoo.animal;

public abstract class BigCat extends Animal {

    public BigCat() {
        this.lifeExpectancy = 24;
        this.eats = new String[] {"steak", "celery"};
    }

    @Override
    public void treat() {
        stroked();
    }

    protected abstract void stroked();
}
