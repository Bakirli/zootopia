package zoo.animal;

import java.util.Arrays;
import java.util.StringJoiner;

public class Giraffe extends Animal {

    public Giraffe() {
        this.lifeExpectancy = 28;
        this.eats = new String[] {"hay", "fruit"};
    }

    @Override
    public void treat() {
        neckMassage();
    }

    private void neckMassage() {
        this.setHealth(this.health + 4);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Giraffe.class.getSimpleName() + "[", "]")
                .add("age=" + age)
                .add("gender='" + gender + "'")
                .add("eats=" + Arrays.toString(eats))
                .add("health=" + health)
                .add("lifeExpectancy=" + lifeExpectancy)
                .toString();
    }
}
