package zoo.enclosure;

import zoo.animal.Animal;
import zoo.food.Foodstore;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Enclosure {
    private List<Animal> animals;
    private Foodstore foodstore;
    private int waste;

    public Enclosure() {
        this.animals = new ArrayList<>();
        this.foodstore = new Foodstore();
    }

    public boolean addAnimal(Animal animal) {
        if(animals.size() < 20) {
            animal.setEnclosure(this);
            animals.add(animal);
            System.out.println("added animal " + animal + " into closure");
            return true;
        } else {
            System.out.println("enclosure is full");
            return false;
        }
    }

    public boolean removeAnimal(Animal animal) {
        animal.setEnclosure(null);
        return animals.remove(animal);
    }

    public void addWaste(int waste) {
        this.waste += waste;
    }

    public void removeWaste(int waste) {
        System.out.println(waste + " waste is removed from enclosure");

        if(this.waste >= waste) {
            this.waste -= waste;
        } else {
            this.waste = 0;
        }
    }

    public int getWasteSize() {
        return this.waste;
    }

    public Foodstore getFoodstore() {
        return this.foodstore;
    }

    public int size() {
        return animals.size();
    }

    public void aMonthPasses() {
        System.out.println("\na month passes in enclosure \n");
        for (Animal a: animals) {
            a.aMonthPasses();
        }
        System.out.println("");
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Enclosure.class.getSimpleName() + "[", "]")
                .add("animals=" + animals)
                .add("foodstore=" + foodstore)
                .add("waste=" + waste)
                .toString();
    }
}
