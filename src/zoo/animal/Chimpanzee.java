package zoo.animal;

import java.util.Arrays;
import java.util.StringJoiner;

public class Chimpanzee extends Ape {

    public Chimpanzee() {
        this.lifeExpectancy = 24;
    }

    @Override
    public void treat() {
        playChase();
    }

    private void playChase() {
        this.setHealth(health + 4);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Chimpanzee.class.getSimpleName() + "[", "]")
                .add("age=" + age)
                .add("gender='" + gender + "'")
                .add("eats=" + Arrays.toString(eats))
                .add("health=" + health)
                .add("lifeExpectancy=" + lifeExpectancy)
                .toString();
    }
}
