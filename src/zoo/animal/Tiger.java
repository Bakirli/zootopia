package zoo.animal;

import java.util.Arrays;
import java.util.StringJoiner;

public class Tiger extends BigCat {

    @Override
    protected void stroked() {
        this.setHealth(health + 3);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Tiger.class.getSimpleName() + "[", "]")
                .add("age=" + age)
                .add("gender='" + gender + "'")
                .add("eats=" + Arrays.toString(eats))
                .add("health=" + health)
                .add("lifeExpectancy=" + lifeExpectancy)
                .toString();
    }
}
