package zoo.food;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FoodMenu {

    private static Map<String, Food> foodMap;

   static {
        foodMap = new HashMap<>();

        Food hay = new Food("hay", 1, 4);
        foodMap.put(hay.getName(), hay);

        Food steak = new Food("steak", 3, 4);
        foodMap.put(steak.getName(), steak);

        Food fruit = new Food("fruit", 2, 3);
        foodMap.put(fruit.getName(), fruit);

        Food celery = new Food("celery", 0,1);
        foodMap.put(celery.getName(), celery);

        Food fish = new Food("fish", 3, 2);
        foodMap.put(fish.getName(), fish);

        Food icecream = new Food("icecream", 1, 3);
        foodMap.put(icecream.getName(), icecream);
    }

    public static Food getFood(String name) {
        return foodMap.get(name);
    }

    public static Set<String> getFoodNames() {
        return foodMap.keySet();
    }
}
