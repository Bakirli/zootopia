package zoo.animal;

import java.util.Arrays;
import java.util.StringJoiner;

public class Elephant extends Animal {

    public Elephant() {
        this.lifeExpectancy = 36;
        this.eats = new String[] {"hay", "fruit"};
    }

    @Override
    public void treat() {
        bath();
    }

    private void bath() {
        System.out.println("elephant is taking bath");
        this.setHealth(health + 5);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Elephant.class.getSimpleName() + "[", "]")
                .add("age=" + age)
                .add("gender='" + gender + "'")
                .add("eats=" + Arrays.toString(eats))
                .add("health=" + health)
                .add("lifeExpectancy=" + lifeExpectancy)
                .toString();
    }
}
