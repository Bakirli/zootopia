package zoo.animal;

import java.util.Arrays;
import java.util.StringJoiner;

public class Gorilla extends Ape {

    public Gorilla() {
        this.lifeExpectancy = 32;
    }

    @Override
    public void treat() {
        painting();
    }

    private void painting() {
        this.setHealth(health + 4);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Gorilla.class.getSimpleName() + "[", "]")
                .add("age=" + age)
                .add("gender='" + gender + "'")
                .add("eats=" + Arrays.toString(eats))
                .add("health=" + health)
                .add("lifeExpectancy=" + lifeExpectancy)
                .toString();
    }
}
